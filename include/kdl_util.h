#ifndef OS_CONTROL_KDL_UTIL_H
#define OS_CONTROL_KDL_UTIL_H

#include <kdl/tree.hpp>

class KDLUtil
{
public:
  static void printKDLTree(KDL::Tree);
  static void printKDLChain(KDL::Chain);
  static void printKDLFrame(KDL::Frame);
  static void printKDLFrameVel(KDL::FrameVel);
  static void printKDLTwist(KDL::Twist);
  static void vectorToJntArray(std::vector<double>, KDL::JntArray&);
  static void vectorToJntArrayVel(std::vector<double>, KDL::JntArray&, KDL::JntArrayVel&);
  template<int N>
  static void eigenToJntArray(Eigen::Matrix<double, N, 1>&, KDL::JntArray&);
};

#endif
