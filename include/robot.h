#ifndef OS_CONTROL_ROBOT_H
#define OS_CONTROL_ROBOT_H

#include <Eigen/Geometry>
#include <vector>
#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Wrench.h>
#include <geometry_msgs/WrenchStamped.h>
#include <sensor_msgs/JointState.h>
#include <os_control/OpSpaceCommand.h>
#include <rtt_ros_kdl_tools/chain_utils.hpp>


template <int NUM_JOINTS>
class Robot
{
private:
  typedef Eigen::Matrix<double, NUM_JOINTS, 1> JointVector;
  typedef Eigen::Matrix<double, 6, 1> CartVector;
  typedef Eigen::Matrix<double, 6, NUM_JOINTS> Jacobian;
  typedef Eigen::Matrix<double, NUM_JOINTS, NUM_JOINTS> JointMassMatrix;
  typedef Eigen::Matrix<double, 6, 6> CartMassMatrix;
  
private:
  ros::NodeHandle nh_;
  std::string ns_; // TODO get from parameter server instead
  ros::Rate rate_;
  ros::Time time_;
  ros::Time prev_time_;
  ros::Duration dt_;
  
  bool DEBUG;
  bool received_command_;
  
  Eigen::Affine3d x_;
  Eigen::Affine3d x_des_;
  Eigen::Affine3d prev_x_des_;
  CartVector xerr_;
  CartVector xdot_;
  CartVector xdot_des_;
  CartVector xdot_err_;
  CartVector wrench_;
  CartVector wrench_des_;
  CartVector Wrench_err_;
  JointVector q_;
  JointVector qdot_;
  Jacobian J_;
  JointMassMatrix Mq_;
  CartMassMatrix Mx_;
  JointVector Gq_;
  JointVector Cq_;

  KDL::Vector gravity_;

  CartVector Kp_;
  CartVector Kd_;

  KDL::JntArray q_temp_;
  KDL::JntArray qdot_temp_;
  KDL::JntArrayVel s_temp_;
  KDL::Jacobian j_temp_;
  KDL::JntSpaceInertiaMatrix mq_temp_;
  KDL::JntArray gq_temp_;
  KDL::JntArray cq_temp_;
  
  // KDL
  rtt_ros_kdl_tools::ChainUtils kdl_;
  KDL::Chain kdl_chain_;
  boost::scoped_ptr<KDL::ChainJntToJacSolver> jac_solver_;
  boost::scoped_ptr<KDL::ChainDynParam> dyn_model_solver_;
  
  // subscribers and publishers
  ros::Subscriber op_space_cmd_sub_;
  ros::Subscriber joint_state_sub_;
  ros::Subscriber wrench_state_sub_;
  ros::Publisher joint_cmd_pub_;

  // message for publishing efforts
  sensor_msgs::JointState joint_cmd_;
  
  void update();
  
  // callback functions
  void opSpaceCmdCallback(os_control::OpSpaceCommand);
  void jointStateCallback(sensor_msgs::JointState);
  void wrenchStateCallback(geometry_msgs::WrenchStamped); // need to be stamped?
  
public:
  Robot(ros::NodeHandle, std::string, int, bool);
  void run();
  void forwardPositionKinematics(JointVector&, Eigen::Affine3d&);
  void forwardVelocityKinematics(JointVector&, JointVector&, CartVector&);
  Jacobian getJacobian(JointVector &q);
  JointMassMatrix getJointMassMatrix(JointVector &q);
  JointVector getJointGravityVector(JointVector &q);
  JointVector getJointCoriolisVector(JointVector &q, JointVector &qdot);
  void computeTaskSpaceDifference(Eigen::Affine3d &x, Eigen::Affine3d &x_des, CartVector &xerr);
};
#endif

