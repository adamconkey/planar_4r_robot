#ifndef OS_CONTROL_EIGEN_UTIL_H
#define OS_CONTROL_EIGEN_UTIL_H

#include <Eigen/Geometry>

class EigenUtil
{
public:
  template<typename Derived>
  static void stdVectorToEigen(std::vector<double> &v,
                               Eigen::MatrixBase<Derived> &m);

  template<typename Derived>
  static void eigenEffortToJointState(const Eigen::MatrixBase<Derived> &e,
                                      sensor_msgs::JointState &js);
  
  template<typename Derived1, typename Derived2>
  static void getPseudoInverse(const Eigen::MatrixBase<Derived1> &m,
                               Eigen::MatrixBase<Derived2> &m_pinv);
  
  template<typename Derived1, typename Derived2>
  static void getPseudoInverse(const Eigen::MatrixBase<Derived1> &m,
                               Eigen::MatrixBase<Derived2> &m_pinv,
                               double tolerance);
};

#endif
