#include "../include/robot.h"
#include <eigen_conversions/eigen_kdl.h>
#include <eigen_conversions/eigen_msg.h>
#include "kdl_util.cpp"
#include "eigen_util.cpp"


using namespace std;


template <int NUM_JOINTS>
Robot<NUM_JOINTS>::Robot(ros::NodeHandle nh, string robot_name, int rate, bool debug):nh_(nh), rate_(rate)
{
  ROS_INFO_STREAM("Initializing operational space controller");
  
  DEBUG = debug;
  received_command_ = false;
  
  // KDL
  bool status = kdl_.init();
  if (DEBUG)
  {
    if (status)
    {
      ROS_INFO_STREAM("Chain init SUCCESS");
      kdl_.printChain();
    } else
    {
      ROS_INFO_STREAM("Chain init FAILURE");
    }
  }
  
  gravity_ = KDL::Vector(0.,0.,-9.81289);
  
  kdl_chain_ = kdl_.getChain();
  jac_solver_.reset(new KDL::ChainJntToJacSolver(kdl_chain_));
  dyn_model_solver_.reset(new KDL::ChainDynParam(kdl_chain_, gravity_));
  
  // initialize temporary KDL objects according to number of joints
  q_temp_.resize(NUM_JOINTS);
  qdot_temp_.resize(NUM_JOINTS);
  j_temp_.resize(NUM_JOINTS);
  mq_temp_.resize(NUM_JOINTS);
  gq_temp_.resize(NUM_JOINTS);
  cq_temp_.resize(NUM_JOINTS);
  
  // initialize gains
  double p = 2.0;
  double d = 0.1;
  Kp_(0) = p;  Kd_(0) = d; // x translation 
  Kp_(1) = p;  Kd_(1) = d; // y translation
  Kp_(2) = p;  Kd_(2) = d; // z translation
  Kp_(3) = p;  Kd_(3) = d; // x rotation
  Kp_(4) = p;  Kd_(4) = d; // y rotation
  Kp_(5) = p;  Kd_(5) = d; // z rotation

  
  // ROS
  ns_ = '/' + robot_name; // TODO is there a better way to get this? launch file/param server?
  
  // subscribers and publishers
  op_space_cmd_sub_ = nh_.subscribe(ns_ + "/op_space_cmd", 100, &Robot::opSpaceCmdCallback, this);
  joint_state_sub_ = nh_.subscribe(ns_ + "/joint_states", 100, &Robot::jointStateCallback, this);
  wrench_state_sub_ = nh_.subscribe(ns_ + "/ft_sensor", 100, &Robot::wrenchStateCallback, this);
  joint_cmd_pub_ = nh_.advertise<sensor_msgs::JointState>(ns_ + "/joint_cmd", 100);
  // TODO check these queue sizes, should it be buffering?

  rtt_ros_kdl_tools::initJointStateFromKDLCHain(kdl_chain_, joint_cmd_);

  prev_time_ = ros::Time::now();  

  ROS_INFO_STREAM("Operational space controller initialization complete.");
}

template <int NUM_JOINTS>
void Robot<NUM_JOINTS>::update()
{
  time_ = ros::Time::now();
  dt_ = time_ - prev_time_;

  this->forwardPositionKinematics(q_, x_);
  this->forwardVelocityKinematics(q_, qdot_, xdot_); // TODO just use kinematics solver for both?

  ROS_WARN_STREAM("Joint angles: \n" << q_);
  
  J_ = this->getJacobian(q_);
  Mq_ = this->getJointMassMatrix(q_);
  Gq_ = this->getJointGravityVector(q_);
  Cq_ = this->getJointCoriolisVector(q_, qdot_);
  
  // compute pose error
  this->computeTaskSpaceDifference(x_, x_des_, xerr_);

  // compute twist error
  this->computeTaskSpaceDifference(prev_x_des_, x_des_, xdot_des_);
  xdot_des_ /= dt_.toSec();
  xdot_err_ = xdot_des_ - xdot_; // TODO is this meaningful, or do you need quaternions?
     
  CartVector ux = Kp_.asDiagonal() * xerr_ + Kd_.asDiagonal() * xdot_err_;
  
  EigenUtil::getPseudoInverse(J_ * Mq_.inverse() * J_.transpose(), Mx_);
  
  JointVector uq = J_.transpose() * Mx_ * ux + Gq_ + Cq_; // TODO try adding coriolis

  
  // TODO add null space component

  
  EigenUtil::eigenEffortToJointState(uq, joint_cmd_);
  joint_cmd_pub_.publish(joint_cmd_);

  prev_x_des_ = x_des_;
  prev_time_ = time_;
}

template <int NUM_JOINTS>
void Robot<NUM_JOINTS>::run()
{
  ROS_INFO_STREAM("Starting control execution. Listening for commands.");
  ros::spinOnce();

  // wait for command
  while(ros::ok() && !received_command_)
  {
    ros::spinOnce();
    rate_.sleep();
  }

  ROS_INFO_STREAM("Command received, initiating control loop.");
  
  while(ros::ok())
  {
    this->update();
    ros::spinOnce();
    rate_.sleep();
  }

  ROS_INFO_STREAM("Control loop complete. Exiting.");
}

template <int NUM_JOINTS>
void Robot<NUM_JOINTS>::forwardPositionKinematics(JointVector &q, Eigen::Affine3d &x)
{
  KDLUtil::eigenToJntArray<NUM_JOINTS>(q, q_temp_);
  tf::transformKDLToEigen(kdl_.jointToCartesian(q_temp_), x);
 
  if (DEBUG)
  {
    ROS_INFO_STREAM("\nTask Space Position:\n" << x.matrix());
  }
}

template <int NUM_JOINTS>
void Robot<NUM_JOINTS>::forwardVelocityKinematics(JointVector &q, JointVector &qdot, CartVector &xdot)
{
  J_ = this->getJacobian(q);
  xdot = J_ * qdot;
  
  if (DEBUG)
  {
    ROS_INFO_STREAM("\nJoint Space Velocity:\n" << qdot.transpose().matrix());
    ROS_INFO_STREAM("\nTask Space Velocity:\n" << xdot.transpose().matrix());
  }
}

template <int NUM_JOINTS>
typename Robot<NUM_JOINTS>::Jacobian Robot<NUM_JOINTS>::getJacobian(JointVector &q)
{
  KDLUtil::eigenToJntArray(q, q_temp_);
  jac_solver_->JntToJac(q_temp_, j_temp_);
  
  if (DEBUG)
  {
    ROS_WARN_STREAM("\nJacobian:\n" << j_temp_.data);
  }
  
  return j_temp_.data;
}

template <int NUM_JOINTS>
typename Robot<NUM_JOINTS>::JointMassMatrix Robot<NUM_JOINTS>::getJointMassMatrix(JointVector &q)
{
  KDLUtil::eigenToJntArray(q, q_temp_);
  dyn_model_solver_->JntToMass(q_temp_, mq_temp_);
  
  if (DEBUG)
  {
    ROS_INFO_STREAM("\nJoint Space Mass Matrix:\n" << mq_temp_.data);
  }

  return mq_temp_.data;
}

template <int NUM_JOINTS>
typename Robot<NUM_JOINTS>::JointVector Robot<NUM_JOINTS>::getJointGravityVector(JointVector &q)
{
  KDLUtil::eigenToJntArray(q, q_temp_);
  dyn_model_solver_->JntToGravity(q_temp_, gq_temp_);

  if (DEBUG)
  {
    ROS_INFO_STREAM("\nJoint Space Gravity Matrix:\n" << gq_temp_.data);
  }

  return gq_temp_.data;
}

template <int NUM_JOINTS>
typename Robot<NUM_JOINTS>::JointVector Robot<NUM_JOINTS>::getJointCoriolisVector(JointVector &q,
                                                                                  JointVector &qdot)
{
  KDLUtil::eigenToJntArray(q, q_temp_);
  KDLUtil::eigenToJntArray(qdot, qdot_temp_);
  dyn_model_solver_->JntToCoriolis(q_temp_, qdot_temp_, cq_temp_);

  if (DEBUG)
  {
    ROS_INFO_STREAM("\nJoint Space Coriolis Matrix:\n" << cq_temp_.data);
  }

  return cq_temp_.data;
}


template <int NUM_JOINTS>
void Robot<NUM_JOINTS>::computeTaskSpaceDifference(Eigen::Affine3d &x,
                                                   Eigen::Affine3d &x_des,
                                                   CartVector &xerr)
{
  // position error
  xerr.head<3>() = x_des.translation() - x.translation();
  // orientation error
  Eigen::Quaterniond q = Eigen::Quaterniond(x.rotation());
  Eigen::Quaterniond qd = Eigen::Quaterniond(x_des.rotation());
  xerr.tail<3>() = q.w()*qd.vec() - qd.w()*q.vec() - qd.vec().cross(q.vec());
}

template <int NUM_JOINTS>
void Robot<NUM_JOINTS>::opSpaceCmdCallback(os_control::OpSpaceCommand cmd_msg)
{
  tf::poseMsgToEigen(cmd_msg.pose, x_des_);
  tf::wrenchMsgToEigen(cmd_msg.wrench, wrench_des_);
  received_command_ = true;
}

template <int NUM_JOINTS>
void Robot<NUM_JOINTS>::jointStateCallback(sensor_msgs::JointState joint_state_msg)
{
  EigenUtil::stdVectorToEigen(joint_state_msg.position, q_);
  EigenUtil::stdVectorToEigen(joint_state_msg.position, qdot_);
}

template <int NUM_JOINTS>
void Robot<NUM_JOINTS>::wrenchStateCallback(geometry_msgs::WrenchStamped wrench_state_msg)
{
  tf::wrenchMsgToEigen(wrench_state_msg.wrench, wrench_);
}


int main(int argc, char** argv)
{
  // ros::init(argc, argv, "test_robot");
  // ros::NodeHandle nh;
  // string robot_name = "planar4R";
  // int rate = 100;
  // Robot<4> robot(nh, robot_name, rate, true);
  
  // //vector<double> p(4, 0.0);
  // vector<double> p(4, 0.78539816339); // pi/4
  // //vector<double> v(4, 0.0);
  // vector<double> v(4, 1.0);
  
  // Eigen::Matrix<double, 4, 1> q;
  // Eigen::Matrix<double, 4, 1> qdot;
  // EigenUtil::stdVectorToEigen(p, q);
  // EigenUtil::stdVectorToEigen(v, qdot);
  
  // Eigen::Affine3d x;
  // Eigen::Matrix<double, 6, 1> xdot;
  
  // robot.forwardPositionKinematics(q, x);
  // robot.forwardVelocityKinematics(q, qdot, xdot);

  ros::init(argc, argv, "robot_op_space_control");
  ros::NodeHandle nh;
  string robot_name = "planar4R";
  int rate = 100;
  Robot<4> robot(nh, robot_name, rate, true);
  robot.run();
}
