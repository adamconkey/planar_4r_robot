import tf
import numpy as np
import sympy as sp


def Rx_matrix(theta, symbolic=False):
    """
    Rotation matrix around the X axis
    """
    if symbolic:
        Rx = sp.Matrix([
            [1, 0, 0],
            [0, sp.cos(theta), -sp.sin(theta)],
            [0, sp.sin(theta), sp.cos(theta)]
        ])
    else:
        Rx = np.array([
            [1, 0, 0],
            [0, np.cos(theta), -np.sin(theta)],
            [0, np.sin(theta), np.cos(theta)]
        ])
    return Rx


def Ry_matrix(theta, symbolic=False):
    """
    Rotation matrix around the Y axis
    """
    if symbolic:
        Ry = sp.Matrix([
            [sp.cos(theta), 0, sp.sin(theta)],
            [0, 1, 0],
            [-sp.sin(theta), 0, sp.cos(theta)]
        ])
    else:
        Ry = np.array([
            [np.cos(theta), 0, np.sin(theta)],
            [0, 1, 0],
            [-np.sin(theta), 0, np.cos(theta)]
        ])
    return Ry


def Rz_matrix(theta, symoblic=False):
    """
    Rotation matrix around the Z axis
    """
    if symbolic:
        Rz = sp.Matrix([
            [sp.cos(theta), -sp.sin(theta), 0],
            [sp.sin(theta), sp.cos(theta), 0],
            [0, 0, 1]
        ])
    else:
        Rz = np.array([
            [np.cos(theta), -np.sin(theta), 0],
            [np.sin(theta), np.cos(theta), 0],
            [0, 0, 1]
        ])
    return Rz


def get_transformation_matrix(a, d, alpha, theta, symbolic=False):
    if symbolic:
        ct = sp.cos(theta)
        st = sp.sin(theta)
        ca = sp.cos(alpha)
        sa = sp.sin(alpha)
        # get rid of floats that are essentially zero
        ct = 0.0 if isinstance(ct, sp.Float) and sp.Abs(ct) < 1e-13 else ct
        st = 0.0 if isinstance(st, sp.Float) and sp.Abs(st) < 1e-13 else st
        ca = 0.0 if isinstance(ca, sp.Float) and sp.Abs(ca) < 1e-13 else ca
        sa = 0.0 if isinstance(sa, sp.Float) and sp.Abs(sa) < 1e-13 else sa
        tf = sp.Matrix([
            [ct, -st * ca,  st * sa, a * ct],
            [st,  ct * ca, -ct * sa, a * st],
            [ 0,       sa,       ca,      d],
            [ 0,        0,        0,      1]
        ])
    else:
        ct = np.cos(theta)
        st = np.sin(theta)
        ca = np.cos(alpha)
        sa = np.sin(alpha)
        tf = np.array([
            [ct, -st * ca,  st * sa, a * ct],
            [st,  ct * ca, -ct * sa, a * st],
            [ 0,       sa,       ca,      d],
            [ 0,        0,        0,      1]
        ])
    return tf


def axis_rotation_matrix(axis, theta, symbolic=False):
    """
    Returns a rotation matrix around the given axis
    """
    [x, y, z] = axis
    if symbolic:
        c = sp.cos(theta)
        s = sp.sin(theta)
        R = sp.Matrix([
            [x**2 + (1 - x**2) * c, x * y * (1 - c) - z * s, x * z * (1 - c) + y * s],
            [x * y * (1 - c) + z * s, y ** 2 + (1 - y**2) * c, y * z * (1 - c) - x * s],
            [x * z * (1 - c) - y * s, y * z * (1 - c) + x * s, z**2 + (1 - z**2) * c]
        ])
    else:
        c = np.cos(theta)
        s = np.sin(theta)
        R = np.array([
            [x**2 + (1 - x**2) * c, x * y * (1 - c) - z * s, x * z * (1 - c) + y * s],
            [x * y * (1 - c) + z * s, y ** 2 + (1 - y**2) * c, y * z * (1 - c) - x * s],
            [x * z * (1 - c) - y * s, y * z * (1 - c) + x * s, z**2 + (1 - z**2) * c]
        ])
    return R


def homogeneous_translation_matrix(t_x, t_y, t_z):
    """
	Returns a translation matrix the homogeneous space
	"""
    T = np.array([
        [1, 0, 0, t_x],
        [0, 1, 0, t_y],
        [0, 0, 1, t_z],
        [0, 0, 0, 1]
    ])
    return T


def from_transformation_matrix(tf_mat):
    """
    Converts a TF matrix to a tuple (trans_vec, rot_mat)
    """
    return (tf_mat[:, -1], tf_mat[:-1, :-1])


def to_transformation_matrix(trans, rot_mat=np.zeros((3, 3))):
    """
    Converts a tuple (trans_vec, rot_mat) to a transformation matrix
    """
    tf = np.eye(4)
    tf[:-1, :-1] = rot_mat
    tf[:-1, -1] = trans
    return tf


def cartesian_to_homogeneous(cartesian_mat, symbolic=False):
    """
    Converts a cartesian matrix to an homogenous matrix
    """
    dim_x, dim_y = cartesian_mat.shape
    if symbolic:
        homogeneous_mat = sp.eye(dim_x + 1)
    else:
        homogeneous_mat = np.eye(dim_x + 1)
    homogeneous_mat[:-1, :-1] = cartesian_mat
    return homogeneous_mat


def cartesian_to_homogeneous_vector(cartesian_vec):
    """
    Converts a cartesian vector to an homogenous vector
    """
    dim_x = cartesian_vec.shape[0]
    homogeneous_vec = np.ones(dim_x + 1)
    homogeneous_vec[:-1] = cartesian_vec
    return homogeneous_vec


def homogeneous_to_cartesian_vector(homogeneous_vec):
    """
    Converts a cartesian vector to an homogenous vector
    """
    return homogeneous_vec[:-1]


def homogeneous_to_cartesian_matrix(homogeneous_mat):
    """
    Converts a cartesian vector to an homogenous matrix
    """
    return homogeneous_mat[:-1, :-1]


def degrees_to_radians(degrees):
    radians = None
    if degrees is not None:
        radians = degrees * np.pi / 180
    return radians


def pose_to_task_coordinates(pose):
    """
    Convert pose message with position and quaternion to a vector
    (x,y,z,r,p,y) for x,y,z postion and r,p,y Euler angles.
    """
    p = pose.position
    q = pose.orientation
    position = np.array([p.x, p.y, p.z])
    quaternion = np.array([q.x, q.y, q.z, q.w])
    euler = tf.transformations.euler_from_quaternion(quaternion)
    return np.hstack((position, euler))
