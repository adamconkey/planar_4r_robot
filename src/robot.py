#!/usr/bin/env python
import os
import yaml
import numpy as np
import sympy as sp
import rospy
import tf
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
from geometry_msgs.msg import Point, Quaternion, Pose, WrenchStamped, Wrench, Vector3
from copy import deepcopy
import file_util, geometry_util


_END_EFFECTOR = 'ee'
_EFFORT = 'e'
_RUN_RATE = 100


class Robot:

    def __init__(self, robot_name, rate=_RUN_RATE, config_path=file_util.get_config_path()):
        config_file = os.path.join(config_path, robot_name + '_definition.yaml')
        with open(config_file, 'r') as f:
            self.config = yaml.safe_load(f)
        self.rate = rate
        self.rp_rate = rospy.Rate(rate)
        self.dt = 1.0 / self.rate
        self.name = self.config['name']
        self.ns = '/%s/' % self.name
        self.dh_params = self.config['dh']
        self.gravity = sp.Matrix(self.config['gravity'])
        self.links = self._init_links()
        self.joint_names = self.config['joint_names']
        self.num_joints = len(self.joint_names)
        self.joint_state = JointState(name=self.joint_names, position=[0.0] * len(self.joint_names))
        self.wrench_state = None
        self.joint_symbols = self._init_joint_symbols()
        self.task_symbols = [sp.Symbol(s) for s in ['x', 'y', 'z', 'r', 'p', 'y']]
        self._init_transformations()
        self._init_jacobians()
        self._init_inertia()
        self._init_gravity()
        # gains
        self.k_p = np.array([120]*6) # x y z r p y
        self.k_d = np.array([0.06]*6) # x y z r p y
        self.k_p_null = np.array([50.0]*4)
        self.k_d_null = np.array([0]*4)

        self.joint_cmd_pub = rospy.Publisher(self.ns + 'joint_cmd', JointState, queue_size=5)
        self.pose_cmd_sub = rospy.Subscriber(self.ns + 'pose_cmd', Pose, self.set_pose_cmd_cb)
        self.wrench_cmd_sub = rospy.Subscriber(self.ns + 'wrench_cmd', WrenchStamped, self.set_wrench_cmd_cb)
        self.joint_state_sub = rospy.Subscriber(self.ns + 'joint_states', JointState, self.set_joint_state_cb)
        self.wrench_state_sub = rospy.Subscriber(self.ns + 'ft_sensor', WrenchStamped, self.set_wrench_state_cb)
        self.pose_cmd = None
        # define default commanded wrench as zeros
        self.wrench_cmd = WrenchStamped(Header(stamp=rospy.Time.now()), Wrench(Vector3(0, 0, 0), Vector3(0, 0, 0)))
        self.prev_x_d = self.forward_kinematics(self.joint_state.position, as_pose=False)

    def __str__(self):
        string = ''
        for link in self.links:
            string += (str(link) + '\n')
        return string

    def forward_kinematics(self, joint_angles, as_pose=False):
        T = self.T[_END_EFFECTOR](*joint_angles)
        position = T[:-1, -1]
        if as_pose:
            position = Point(*position)
            quaternion = Quaternion(*tf.transformations.quaternion_from_matrix(T))
            pose = Pose(position, quaternion)
        else:
            orientation = np.array(tf.transformations.quaternion_from_matrix(T))
            pose = np.hstack((position, orientation))
        return pose

    def get_task_space_trajectory(self, joint_trajectory):
        ee_trajectory = []
        for waypoint in joint_trajectory:
            pose = self.forward_kinematics(waypoint, as_pose=True)
            ee_trajectory.append(pose)
        return ee_trajectory

    def _quaternion_difference(self, q_d, q_a):
        skew = np.array([[0, -q_d[2], q_d[1]],
                         [q_d[2], 0, -q_d[0]],
                         [-q_d[1], q_d[0], 0]])
        diff = q_a[3]*q_d[:3] - q_d[3]*q_a[:3] - np.dot(skew, q_a[:3])
        return diff

    def compute_jacobian(self, q, frame):
        J = self.J[frame](*q)
        return J

    def set_pose_cmd_cb(self, pose_cmd_msg):
        self.pose_cmd = pose_cmd_msg

    def get_pose_cmd(self):
        pose_cmd = deepcopy(self.pose_cmd)
        return pose_cmd

    def set_wrench_cmd_cb(self, wrench_cmd_msg):
        self.wrench_cmd = wrench_cmd_msg

    def get_wrench_cmd(self):
        wrench_cmd = deepcopy(self.wrench_cmd)
        return wrench_cmd

    def set_joint_state_cb(self, joint_state_msg):
        self.joint_state = joint_state_msg

    def get_joint_state(self):
        joint_state = deepcopy(self.joint_state)
        return joint_state

    def set_wrench_state_cb(self, wrench_state_msg):
        self.wrench_state = wrench_state_msg

    def control_force_position_ns(self):
        joint_state = self.get_joint_state()
        wrench_cmd = self.get_wrench_cmd()

        # joint space components
        q = joint_state.position
        J = self.J[_END_EFFECTOR](*q)
        M_q = self.M(*q)
        G = self.G(*q)
        dq = np.array(joint_state.velocity)

        # task space components
        x = self.forward_kinematics(q)
        x_d = self._get_desired_ee_pose()
        dx = np.dot(J, dq)
        dx_d = self._get_desired_ee_velocity(x_d)
        ee_velocity_error = dx_d - dx
        ee_pose_error = self._get_ee_pose_error(x, x_d)
        u_x = self.k_p * ee_pose_error + self.k_d * ee_velocity_error

        M_x_inv = np.dot(J, np.dot(np.linalg.inv(M_q), J.T))
        U, s, Vt = np.linalg.svd(M_x_inv)
        V = Vt.T  # numpy returns the transpose
        # cut off singular values (check this if there are problems)
        s_inv = np.zeros(s.shape)
        for i in range(s.shape[0]):
            s_inv[i] = 0 if s[i] < 1e-5 else 1.0 / s[i]
        s_inv = np.diag(s_inv)
        M_x = np.dot(V, np.dot(s_inv, U.T))  # inverse equals V*inv(s)*transpose(U)

        u = np.dot(J.T, np.dot(M_x, u_x)).reshape((self.num_joints, 1)) + G

        # null space control keeping joint angles reasonable
        gen_inv = np.dot(M_x, np.dot(J, np.linalg.inv(M_q)))
        null_filter = np.eye(self.num_joints) - np.dot(J.T, gen_inv)
        q_d_null = np.array([np.pi / 4] * self.num_joints)
        dq_d_null = dq
        u_null = np.dot(M_q,
                        self.k_p_null * (q_d_null - q) + self.k_d_null * dq_d_null
                        ).reshape((self.num_joints, 1))
        u += np.dot(null_filter, u_null)

        joint_cmd = JointState()
        joint_cmd.name = self.joint_names
        joint_cmd.effort = u
        # publish effort to ROS controller
        self.joint_cmd_pub.publish(joint_cmd)

    def _get_desired_ee_pose(self):
        pose_cmd = self.get_pose_cmd()
        pos = pose_cmd.position
        quat = pose_cmd.orientation
        x_d = np.array([pos.x, pos.y, pos.z, quat.x, quat.y, quat.z, quat.w])
        return x_d

    def _get_desired_ee_velocity(self, x_d):
        dx_d_pos = x_d[:3] - self.prev_x_d[:3]
        dx_d_orient = self._quaternion_difference(x_d[3:], self.prev_x_d[3:])
        dx_d = np.hstack((dx_d_pos, dx_d_orient)) / self.dt
        return dx_d

    def _get_ee_pose_error(self, x, x_d):
        position_error = x_d[:3] - x[:3]
        q_d = np.array(x_d[3:])
        q_a = np.array(x[3:])
        orientation_error = self._quaternion_difference(q_d, q_a)
        ee_pose_error = np.hstack((position_error, orientation_error))
        return ee_pose_error

    def _init_links(self):
        links = []
        for link_name in sorted(self.dh_params.keys()):
            dh = self.dh_params[link_name]
            link = Link(link_name, dh['a'], dh['d'], dh['alpha'], dh['theta'],
                        dh['length'], dh['mass'], dh['ixx'], dh['iyy'], dh['izz'])
            links.append(link)
        return links

    def _init_joint_symbols(self):
        symbols = []
        for link in self.links:
            for symbol in link.symbols:
                symbols.append(symbol)
        return symbols

    def _init_transformations(self):
        self.T_sym = {}
        self.T = {}
        T = None
        for link in self.links:
            filename = '%s_%s.T' % (self.name, link.name)
            # see if there's a cached version to load
            T_link = file_util.get_cached_object(filename)
            if T_link is None:
                T = link.T if T is None else T * link.T

                # need to check if float errors are being introduced
                for i in range(T.shape[0]):
                    for j in range(T.shape[1]):
                        if isinstance(T[i,j], sp.Float) and sp.Abs(T[i,j]) < 1e-13:
                            T[i,j] = 0.0
                            
                T_link = sp.simplify(T)
                file_util.cache_object(T_link, filename)
            self.T_sym[link.name] = T_link
            self.T[link.name] = sp.lambdify(self.joint_symbols, T_link)

    def _init_jacobians(self):
        self.J_sym = {}
        self.J = {}
        orientation = sp.Matrix([0.0, 0.0, 0.0])
        for i, link in enumerate(self.links):
            filename = '%s_%s.J' % (self.name, link.name)
            J = file_util.get_cached_object(filename)
            if J is None:
                T = self.T_sym[link.name]
                origin = sp.Matrix([0, 0, 0, 1])
                origin_position = sp.Matrix((T * origin)[:-1])
                orientation += (T[:3, :3] * link.orientation)
                origin_pose = origin_position.col_join(orientation)
                J = origin_pose.jacobian(self.joint_symbols)
                file_util.cache_object(J, filename)
            self.J_sym[link.name] = J
            self.J[link.name] = sp.lambdify(self.joint_symbols, J)

            # jacobians for COM of each link, load if cached
            filename = '%s_%s_com.J' % (self.name, link.name)
            J_com = file_util.get_cached_object(filename)
            if J_com is None:
                T = self.T_sym[link.name]
                com = link.com.col_join(sp.Matrix([1]))
                com_position = sp.Matrix((T * com)[:-1])
                com_pose = com_position.col_join(orientation)
                J_com = com_pose.jacobian(self.joint_symbols)
                file_util.cache_object(J_com, filename)
            self.J_sym[link.name + 'com'] = J_com
            self.J[link.name + 'com'] = sp.lambdify(self.joint_symbols, J_com)

    def _init_inertia(self):
        filename = '%s.M' % self.name
        M = file_util.get_cached_object(filename)
        if M is None:
            M = sp.zeros(self.num_joints)
            for i, link in enumerate(self.links):
                J = self.J_sym[link.name + 'com']
                M += J.T * link.M * J
            file_util.cache_object(M, filename)
        # assuming only lambda version is needed
        self.M = sp.lambdify(self.joint_symbols, M)

    def _init_gravity(self):
        filename = '%s.G' % self.name
        G = file_util.get_cached_object(filename)
        if G is None:
            G = sp.zeros(self.num_joints, 1)
            for i, link in enumerate(self.links):
                G += self.J_sym[link.name + 'com'].T * link.M * self.gravity
            # G = sp.simplify(G)
            file_util.cache_object(G, filename)
        # assuming only lambda version is needed            
        self.G = sp.lambdify(self.joint_symbols, G)

    def spin(self):
        """
        Control pose if command has been sent, otherwise just wait for one.
        """
        while not rospy.is_shutdown():
            if self.pose_cmd is not None:
                self.control_force_position_ns()
            self.rp_rate.sleep()


class Link:
    """
    Link in Denavit-Hartenberg representation.

    name (str): name of the link
    d (flt): offset along previous z axis
    a (flt): offset along current x axis
    alpha (flt): rotation about current x axis
    theta (flt): rotation about previous z axis
    """
    def __init__(self, name, a=0.0, d=0.0, alpha=None, theta=None, length=None,
                 mass=None, ixx=None, iyy=None, izz=None):
        self.name = str(name)
        self.a = a
        self.d = d
        self.alpha = geometry_util.degrees_to_radians(alpha)
        self.theta = geometry_util.degrees_to_radians(theta)
        self.symbols = []
        self.length = length
        if self.alpha is None:
            self.alpha = sp.Symbol('alpha_%s' % self.name)
            self.symbols.append(self.alpha)
        if self.theta is None:
            self.theta = sp.Symbol('theta_%s' % self.name)
            self.symbols.append(self.theta)
            # simplifying this for now, probably want more robust way of doing it
            self.com = sp.Matrix([self.length * 0.5 * sp.cos(self.theta),
                                  self.length * 0.5 * sp.sin(self.theta),
                                  0.0])
            self.orientation = sp.Matrix([0.0, 0.0, self.theta])
        else:
            self.theta = theta
            self.com = sp.Matrix([self.length * 0.5 * np.cos(self.theta),
                                  self.length * 0.5 * np.sin(self.theta),
                                  0.0])
            self.orientation = sp.Matrix([0.0, 0.0, 0.0])
            
        self.mass = mass
        self.M = np.diag([mass, mass, mass, ixx, iyy, izz])
        self.T = geometry_util.get_transformation_matrix(self.a, self.d, self.alpha,
                                                         self.theta, True)
        
    def __str__(self):
        template = 'Link: name=%s, a=%.2f, d=%.2f, alpha=%s, theta=%s'
        params = (self.name, self.a, self.d, str(self.alpha), str(self.theta))
        return template % params
    
    
def test_robot_config():
    rospy.init_node('test_robot_config')
    robot = Robot('planar4R')
    print str(robot)
    for link in robot.links:
        sp.pprint(link.T)


def test_jacobian():
    rospy.init_node('test_jacobian')
    robot = Robot('planar4R')
    angles = [0.926982, 0.355445, 1.04118, 0.209586]
    J = robot.compute_jacobian(angles, '3')
    print J
    

if __name__ == '__main__':
    # rospy.init_node('robot_w_op_space_control')
    # robot = Robot('planar4R')
    # robot.spin()
    test_jacobian()
