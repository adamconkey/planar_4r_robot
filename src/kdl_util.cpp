#include "../include/kdl_util.h"


using namespace std;


void KDLUtil::vectorToJntArray(vector<double> v, KDL::JntArray &j)
{
  for (int i = 0; i < v.size(); i++)
    j(i) = v[i];
}

template<int N>
void KDLUtil::eigenToJntArray(Eigen::Matrix<double, N, 1> &e, KDL::JntArray &j)
{
  for (int i = 0; i < N; i++)
    j(i) = e[i];
}

void KDLUtil::vectorToJntArrayVel(vector<double> v, KDL::JntArray &jp, KDL::JntArrayVel &jv)
{
  KDL::JntArray jtemp;
  KDLUtil::vectorToJntArray(v, jtemp);
  jv = KDL::JntArrayVel(jp, jtemp);
}

void KDLUtil::printKDLTree(KDL::Tree kdl_tree)
{
  cout << "\nKDL Tree Info:" << endl;
  cout << "  Number of joints:   " << kdl_tree.getNrOfJoints() << endl;
  cout << "  Number of segments: " << kdl_tree.getNrOfSegments() << endl;
  cout << "  Link, Joint:" << endl;
  const KDL::SegmentMap segmentMap = kdl_tree.getSegments();
  for (KDL::SegmentMap::const_iterator it = segmentMap.begin(); it != segmentMap.end(); it++)
  {
    cout << "    " << it->second.segment.getName() << ", " << it->second.segment.getJoint().getName() << endl;
  }
}

void KDLUtil::printKDLChain(KDL::Chain kdl_chain)
{
  cout << "\nKDL Chain Info:" << endl;
  cout << "  Number of joints in chain: " << kdl_chain.getNrOfJoints() << endl;
  cout << "  Number of links in chain:  " << kdl_chain.getNrOfSegments() << endl;
}

void KDLUtil::printKDLFrame(KDL::Frame kdl_frame)
{
  cout << "\nKDL Frame:" << endl;
  for (unsigned int i = 0; i < 4; i++)
  {
    for (unsigned int j = 0; j < 4; j++)
    {
      double val = kdl_frame(i,j);
      if (val < 0.001 && val > -0.001)
      {
        val = 0;
      }
      cout << setw(14) << setprecision(4) << val;
    }
    cout << endl;
  }
}

void KDLUtil::printKDLFrameVel(KDL::FrameVel kdl_frame_vel)
{
  KDL::Twist t = kdl_frame_vel.deriv();
  KDLUtil::printKDLTwist(t);
}

void KDLUtil::printKDLTwist(KDL::Twist t)
{
  cout << "\nTwist:" << endl;
  cout << "  (dx, dy, dz, dwx, dwy, dwz) = ";
  cout << "(" << t.vel.x() << ", " << t.vel.y() << ", " << t.vel.z() << ", "
       << t.rot.x() << ", " << t.rot.y() << ", " << t.rot.z() << ")" << endl;
}
