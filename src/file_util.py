import os
import errno
import pickle


def make_dir(new_dir):
    try:
        os.makedirs(new_dir)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def get_config_path():
    self_path = os.path.dirname(os.path.realpath(__file__))
    rel_path = os.path.join(self_path, '../config')
    abs_path = os.path.abspath(rel_path)
    make_dir(abs_path)
    return abs_path


def get_cached_path():
    config_path = get_config_path()
    cached_path = os.path.join(config_path, 'cached')
    make_dir(cached_path)
    return cached_path


def get_cached_object(filename):
    cached_path = get_cached_path()
    cached_object = None
    if os.path.isfile(os.path.join(cached_path, filename)):
        cached_file = os.path.join(cached_path, filename)
        with open(cached_file, 'rb') as f:
            cached_object = pickle.load(f)
    return cached_object


def cache_object(obj, filename):
    cached_path = get_cached_path()
    out_file = os.path.join(cached_path, filename)
    with open(out_file, 'wb') as f:
        pickle.dump(obj, f)

