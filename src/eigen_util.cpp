#include "../include/eigen_util.h"


using namespace Eigen;
using namespace std;


template<typename Derived>
void EigenUtil::stdVectorToEigen(vector<double> &v, MatrixBase<Derived> &m)
{
  for(int i = 0; i < v.size(); i++)
    m[i] = v[i];
}

template<typename Derived>
void EigenUtil::eigenEffortToJointState(const Eigen::MatrixBase<Derived> &e,
                                        sensor_msgs::JointState &js)
{
  for(int i = 0; i < e.rows(); i++)
    js.effort[i] = e[i];
}


template<typename Derived1, typename Derived2>
void EigenUtil::getPseudoInverse(const MatrixBase<Derived1> &m, MatrixBase<Derived2> &m_pinv)
{
  double tolerance = 1.e-5;
  EigenUtil::getPseudoInverse(m, m_pinv, tolerance);
}

template<typename Derived1, typename Derived2>
void EigenUtil::getPseudoInverse(const MatrixBase<Derived1> &m, MatrixBase<Derived2> &m_pinv, double tolerance)
{
  JacobiSVD<typename Derived1::PlainObject> svd(m, ComputeFullU | ComputeFullV);
  typename JacobiSVD<typename Derived1::PlainObject>::SingularValuesType sing_vals = svd.singularValues();
  // set values within tolerance to zero
  for (int idx = 0; idx < sing_vals.size(); idx++)
  {
    if (tolerance > 0.0 && sing_vals(idx) > tolerance)
      sing_vals(idx) = 1.0 / sing_vals(idx);
    else
      sing_vals(idx) = 0.0;
  }
  
  m_pinv = svd.matrixV().leftCols(sing_vals.size()) * sing_vals.asDiagonal() * svd.matrixU().leftCols(sing_vals.size()).transpose();
}
