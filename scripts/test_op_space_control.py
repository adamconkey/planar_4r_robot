#!/usr/bin/env python
import sys
import rospy
import numpy as np
from geometry_msgs.msg import Pose
from os_control.msg import OpSpaceCommand
sys.path.append("../src/")
from robot import Robot


def test_operational_space_control():
    rospy.init_node('test_op_space_control')
    # robot used only to get trajectory to publish, active robot should be running elsewhere
    robot = Robot('planar4R')
    op_space_cmd_pub = rospy.Publisher('/planar4R/op_space_cmd', OpSpaceCommand, queue_size=5)

    # generate a task space trajectory to move to neutral position
    joint_trajectory = []
    values = np.linspace(0, np.pi / 4, 1000)
    for i in range(len(values)):
        joint_trajectory.append([values[i]]*4)
    task_space_trajectory = robot.get_task_space_trajectory(joint_trajectory)
    
    for pose in task_space_trajectory:
        op_cmd = OpSpaceCommand()
        op_cmd.pose = pose
        op_space_cmd_pub.publish(op_cmd)
        robot.rp_rate.sleep()

    # continue to publish most recent pose forever
    while not rospy.is_shutdown():
        op_space_cmd_pub.publish(op_cmd)
        robot.rp_rate.sleep()


if __name__ == '__main__':
    test_operational_space_control()
