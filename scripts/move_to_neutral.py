#!/usr/bin/env python
import numpy as np
import rospy
from std_msgs.msg import Char
from sensor_msgs.msg import JointState


def move_to_neutral():
    rospy.init_node("planar4R_move_to_neutral")
    
    run_rate = rospy.Rate(100)

    rospy.sleep(5.0) # make sure controllers load, TODO make more robust

    joint_cmd_pub = rospy.Publisher('/planar4R/joint_cmd', JointState, queue_size=5)
    
    jc = JointState()
    jc.name = ['joint_0', 'joint_1', 'joint_2', 'joint_3']
    jc.position = [0.0, 0.0, 0.0, 0.0]

    while jc.position[0] < np.pi / 4:
        joint_cmd_pub.publish(jc)
        for j in range(len(jc.position)):
            jc.position[j] += 0.001
        run_rate.sleep()

    jc.position = [np.pi / 4, np.pi / 4, np.pi / 4, np.pi / 4]
    joint_cmd_pub.publish(jc)
    run_rate.sleep()
    

if __name__=='__main__':
    move_to_neutral()
