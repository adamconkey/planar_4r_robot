import rospy
import random
import numpy as np
from std_msgs.msg import Char
from sensor_msgs.msg import JointState
from scipy.interpolate import interp1d
from robot_config import Robot


class TrajectoryCommander:

    def __init__(self, robot, lims, rate=100):
        self.robot = robot
        self.lims = lims
        self.rate = rate
        self.rp_rate = rospy.Rate(rate)
        
    def execute_random_trajectories(self, num_traj=60, spl_ord='cubic', dur=10):
        ts = self._get_random_js_trajectories(num_traj, spl_ord, dur)
        for i, t in enumerate(ts):
            print 'Trajectory %d' % (i+1)
            self._execute_trajectory(t)
            rospy.sleep(2.0) # add buffer time between each trajectory

    def _execute_trajectory(self, t):    
        num_joints, num_pnts = t.shape
        jc = JointState()
        jc.name = self.robot.joint_names

        print '\tFirst point:', t[:,0]
        print '\tLast point:', t[:,-1]
        
        for i in range(num_pnts):
            jc.position = t[:,i]
            self.robot.joint_cmd_pub.publish(jc)
            self.rp_rate.sleep()

    def _get_random_js_trajectories(self, num_traj, spl_ord, dur):
        if spl_ord == 'cubic':
            num_pnts = 4
        else:
            raise ValueError('Current support for cubic splines only.')
        
        qs = self._get_random_js_points(num_traj, num_pnts)
        x = np.linspace(0, dur, self.rate * dur)
        x_init = np.linspace(0, dur, num_pnts)
        ts = []
        for i in np.arange(0, num_traj * (num_pnts-1), num_pnts-1):
            pnts = np.array(qs[i:i+num_pnts]).T
            fs = [interp1d(x_init, pnts[j,:], kind='cubic') for j in range(pnts.shape[0])]
            t = [[f(xi) for xi in x] for f in fs]
            ts.append(np.array(t))
        return ts

    def _get_random_js_points(self, num_traj, num_pnts):
        qs = []
        for i in range(num_traj * num_pnts):
            q = [random.uniform(a,b) for (a,b) in self.lims]
            # check if this puts it below ground plane, reject if so (not ideal but temp hack)
            pose = robot.forward_kinematics(q)
            if pose[2] < 0.05:
                i -= 1
                continue
            qs.append(q)
        print len(qs)
        return qs
    

if __name__=='__main__':
    rospy.init_node('trajectory_generator')
    lims = [
        np.array([0, np.pi]),
        np.array([-3*np.pi/4, 3*np.pi/4]),
        np.array([-3*np.pi/4, 3*np.pi/4]),
        np.array([-3*np.pi/4, 3*np.pi/4]),
    ]
    robot = Robot('planar4R')
    tc = TrajectoryCommander(robot, lims)
    tc.execute_random_trajectories(3)
